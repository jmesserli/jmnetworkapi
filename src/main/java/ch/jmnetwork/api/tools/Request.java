package ch.jmnetwork.api.tools;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Request {

    private String url;
    private RequestType type;
    private Params params;
    private Params headers;
    private String output;

    private Request(Builder b) {
        if (b.url == null) throw new RequestException("Request must have URL!");

        this.url = b.url;
        this.type = b.type == null ? RequestType.GET : b.type;

        // Can be null!
        this.params = b.params;
        this.output = b.output;
        this.headers = b.headers;
    }

    public RequestType getType() {
        return type;
    }

    public String getFullUrl() {
        return url + (params == null ? "" : "?" + params.getParamString());
    }

    public Response perform() {
        URL reqURL;
        HttpURLConnection connection;
        OutputStream os;

        try {
            reqURL = new URL(url + (type == RequestType.GET || type == RequestType.DELETE ? params != null ? "?" + params
                    .getParamString() : "" : ""));
        } catch (MalformedURLException e) {
            throw new RequestException("Malformed URL! (" + url + ")", e);
        }

        try {
            connection = (HttpURLConnection) reqURL.openConnection();
        } catch (IOException e) {
            throw new RequestException("Cannot open connection to " + url, e);
        }

        try {
            connection.setRequestMethod(type.name());
        } catch (ProtocolException e) {
            throw new RequestException("Cannot set request method to " + type.name(), e);
        }

        if (headers != null)
            headers.setRequestProperties(connection);

        if (type == RequestType.POST || type == RequestType.PUT) try {
            connection.setDoOutput(true);
            os = connection.getOutputStream();
            if (output != null) os.write(output.getBytes("UTF-8"));
            else if (params != null)
                os.write(params.getParamString().getBytes("UTF-8")); // getBytes can throw exception
        } catch (IOException e) {
            throw new RequestException("Cannot open outputstream!", e);
        }

        try {
            if ((connection.getResponseCode() + "").matches("[23]..")) {
                return new Response(connection.getResponseCode(), connection.getInputStream());
            } else {
                return new Response(connection.getResponseCode(), connection.getErrorStream());
            }

        } catch (IOException e) {
            throw new RequestException("Cannot read response code / open inputstream", e);
        }

    }

    /////////////////////////////////////////////////////////////////////
    // CLASSES

    public static class Builder {
        private String url;
        private RequestType type;
        private Params params;
        private Params headers;
        private String output;

        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        public Builder setHeaders(Params headers) {
            this.headers = headers;
            return this;
        }

        public Builder setType(RequestType type) {
            this.type = type;
            return this;
        }

        public Builder setParams(Params params) {
            this.params = params;
            return this;
        }

        public Builder setOutput(String output) {
            this.output = output;
            return this;
        }

        public Request build() {
            return new Request(this);
        }
    }

    public static class Params {
        public HashMap<String, String> paramMap;

        private Params(String... params) {
            if (params.length % 2 != 0) throw new RuntimeException("params.length is not a multiple of two!");
            paramMap = new HashMap<>();


            for (int i = 0; i < params.length - 1; i += 2) {
                paramMap.put(params[i], params[i + 1]);
            }
        }

        public static Params fromList(List<String> list) {
            return new Params(list.toArray(new String[list.size()]));
        }

        public static Params fromMap(HashMap<String, String> paramMap) {
            ArrayList<String> list = new ArrayList<>();

            for (String s : paramMap.keySet()) {
                list.add(s);
                list.add(paramMap.get(s));
            }

            return new Params(list.toArray(new String[list.size()]));
        }

        public static Params fromStrings(String... params) {
            return new Params(params);
        }

        public String getParamString() {
            StringBuilder sb = new StringBuilder();

            for (String key : paramMap.keySet()) {
                try {
                    sb.append(key).append("=").append(URLEncoder.encode(paramMap.get(key), "UTF-8")).append("&");
                } catch (UnsupportedEncodingException e) {
                    throw new RequestException("Fail: encoding UTF-8 not supported!", e);
                }
            }

            return sb.toString().replaceAll("&$", "");
        }

        public void setRequestProperties(URLConnection connection) {
            for (String key : paramMap.keySet()) {
                connection.setRequestProperty(key, paramMap.get(key));
            }
        }
    }

    public static class Response {
        public final int responsecode;
        public InputStream inputStream;
        private String input_read = null;
        private String in;

        public Response(int responsecode, InputStream inputStream) {
            this.responsecode = responsecode;
            this.inputStream = inputStream;

        }

        public String readInput() {
            if (input_read != null) return input_read;

            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            try {
                while ((in = br.readLine()) != null) {
                    sb.append(in);
                }
            } catch (IOException e) {
                throw new RequestException("Cannot read input", e);
            }

            input_read = sb.toString();
            try {
                br.close();
            } catch (IOException e) {
                throw new RequestException(e);
            }
            return input_read;
        }

    }

    private static class RequestException extends RuntimeException {
        public RequestException(String message) {
            super(message);
        }

        public RequestException(String msg, Throwable t) {
            super(msg, t);
        }

        public RequestException(Throwable t) {
            super(t);
        }
    }

    public static enum RequestType {
        GET, POST, PUT, DELETE
    }
}
